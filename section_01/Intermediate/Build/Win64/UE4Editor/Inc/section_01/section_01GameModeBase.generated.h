// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SECTION_01_section_01GameModeBase_generated_h
#error "section_01GameModeBase.generated.h already included, missing '#pragma once' in section_01GameModeBase.h"
#endif
#define SECTION_01_section_01GameModeBase_generated_h

#define section_01_Source_section_01_section_01GameModeBase_h_15_RPC_WRAPPERS
#define section_01_Source_section_01_section_01GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define section_01_Source_section_01_section_01GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAsection_01GameModeBase(); \
	friend SECTION_01_API class UClass* Z_Construct_UClass_Asection_01GameModeBase(); \
public: \
	DECLARE_CLASS(Asection_01GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/section_01"), NO_API) \
	DECLARE_SERIALIZER(Asection_01GameModeBase) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define section_01_Source_section_01_section_01GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAsection_01GameModeBase(); \
	friend SECTION_01_API class UClass* Z_Construct_UClass_Asection_01GameModeBase(); \
public: \
	DECLARE_CLASS(Asection_01GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/section_01"), NO_API) \
	DECLARE_SERIALIZER(Asection_01GameModeBase) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define section_01_Source_section_01_section_01GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API Asection_01GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Asection_01GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Asection_01GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Asection_01GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Asection_01GameModeBase(Asection_01GameModeBase&&); \
	NO_API Asection_01GameModeBase(const Asection_01GameModeBase&); \
public:


#define section_01_Source_section_01_section_01GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API Asection_01GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Asection_01GameModeBase(Asection_01GameModeBase&&); \
	NO_API Asection_01GameModeBase(const Asection_01GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Asection_01GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Asection_01GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Asection_01GameModeBase)


#define section_01_Source_section_01_section_01GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define section_01_Source_section_01_section_01GameModeBase_h_12_PROLOG
#define section_01_Source_section_01_section_01GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	section_01_Source_section_01_section_01GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	section_01_Source_section_01_section_01GameModeBase_h_15_RPC_WRAPPERS \
	section_01_Source_section_01_section_01GameModeBase_h_15_INCLASS \
	section_01_Source_section_01_section_01GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define section_01_Source_section_01_section_01GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	section_01_Source_section_01_section_01GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	section_01_Source_section_01_section_01GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	section_01_Source_section_01_section_01GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	section_01_Source_section_01_section_01GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID section_01_Source_section_01_section_01GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
